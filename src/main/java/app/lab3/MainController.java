package app.lab3;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class MainController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button buttonLoad;

    @FXML
    private Button buttonSave;

    @FXML
    private Button buttonUser;

    @FXML
    private Button buttonUser1;

    @FXML
    private Label labelErrorBottom;

    @FXML
    private Label labelErrorUp;

    @FXML
    private TextArea textArea;

    @FXML
    private TextField textFieldUser;

    @FXML
    private TextField textFieldAuthor;

    @FXML
    private ListView<String> listView;

    private Stage mainStage;
    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    private Certificates certificates;
    private String chosen;
//    private HashMap<String, String> map;

    @FXML
    void onActionAbout(ActionEvent event) {
        Stage stage = new Stage();

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("about-page.fxml"));
            Scene scene = new Scene(fxmlLoader.load());

            AboutController controller = fxmlLoader.getController();
            controller.setMainStage(mainStage);
            controller.setThisStage(stage);

            stage.setTitle("О приложении");
            stage.setScene(scene);
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void onActionDelete(ActionEvent event) {
        certificates.deleteCertificate(textFieldUser.getText());

        textFieldAuthor.setText("");
        textFieldUser.setText("");
        textArea.setText("");

        textArea.setDisable(true);
        buttonSave.setDisable(true);
        buttonUser1.setDisable(true);
        buttonLoad.setDisable(true);

        labelErrorBottom.setText("Сертификат успешно удален");
        labelErrorBottom.setTextFill(Paint.valueOf("GREEN"));
        labelErrorBottom.setAlignment(Pos.TOP_CENTER);
    }

    @FXML
    void onActionExit(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    void onActionLoad(ActionEvent event) {
        labelErrorBottom.setText("");
        textArea.setText("");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите подписанный файл");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Подписанный документ(.sd)", "*.sd"));
        File defaultDirectory = new File("./");
        fileChooser.setInitialDirectory(defaultDirectory);
        File selectedFile = fileChooser.showOpenDialog(new Stage());

        if (selectedFile == null) {
            return;
        }

        String author = certificates.checkSignedDoc(selectedFile.getPath());
        if (author.equals("")) {
            labelErrorBottom.setText("Сертификат поврежден или не существует");
            labelErrorBottom.setAlignment(Pos.CENTER);
            labelErrorBottom.setTextFill(Paint.valueOf("RED"));
            return;
        }

        textFieldAuthor.setText(author);
        textArea.setText(certificates.getTextFromSignedDoc(selectedFile.getPath()));
    }

    boolean flagSave = false;
    @FXML
    void onActionSave(ActionEvent event) {
        labelErrorBottom.setText("");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберите директорию и имя файла");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Подписанный документ(.sd)", "*.sd"));
        File defaultDirectory = new File("./");
        fileChooser.setInitialDirectory(defaultDirectory);
        File selectedFile = fileChooser.showSaveDialog(new Stage());

        if (selectedFile == null) {
            return;
        }

        if (!certificates.signDoc(chosen, textArea.getText(), selectedFile.getPath())) {
            labelErrorBottom.setTextFill(Paint.valueOf("RED"));
            labelErrorBottom.setText("Документ не подписан, возможно, поврежден сертификат");
            labelErrorBottom.setAlignment(Pos.CENTER);
        } else {
            labelErrorBottom.setTextFill(Paint.valueOf("GREEN"));
            labelErrorBottom.setText("Документ успешно подписан");
            labelErrorBottom.setAlignment(Pos.CENTER);
            textArea.setText("");
        }
    }

    boolean flag = false;
    @FXML
    void onActionSelectUser(ActionEvent event) {
        labelErrorBottom.setText("");
        textArea.setText("");

        if (!flag) {
            certificates.update();
            ArrayList<String> res = new ArrayList<>();
            listView.setVisible(true);
            buttonUser1.setVisible(true);

            while (certificates.data.hasMoreElements()) {
                String name = certificates.data.nextElement();
                res.add(certificates.getDecodedString(name));

//                System.out.println(certificates.hasIt(certificates.getDecodedString(name)));
            }

            listView.setItems(FXCollections.observableArrayList(res));
        } else {
            chosen = listView.getSelectionModel().getSelectedItems().toString();
            chosen = chosen.substring(1, chosen.length()-1);
            textFieldUser.setText(chosen);

            if (!chosen.equals("")) {
                textArea.setDisable(false);
                buttonLoad.setDisable(false);
                buttonSave.setDisable(false);
            } else {
                textArea.setDisable(true);
                buttonLoad.setDisable(true);
                buttonSave.setDisable(true);
            }

            listView.setVisible(false);
            buttonUser1.setVisible(false);
        }
        flag = !flag;
    }

    @FXML
    void initialize() {
        assert buttonLoad != null : "fx:id=\"buttonLoad\" was not injected: check your FXML file 'main-page.fxml'.";
        assert buttonSave != null : "fx:id=\"buttonSave\" was not injected: check your FXML file 'main-page.fxml'.";
        assert buttonUser != null : "fx:id=\"buttonUser\" was not injected: check your FXML file 'main-page.fxml'.";
        assert labelErrorBottom != null : "fx:id=\"labelErrorBottom\" was not injected: check your FXML file 'main-page.fxml'.";
        assert labelErrorUp != null : "fx:id=\"labelErrorUp\" was not injected: check your FXML file 'main-page.fxml'.";
        assert textArea != null : "fx:id=\"textArea\" was not injected: check your FXML file 'main-page.fxml'.";
        assert textFieldUser != null : "fx:id=\"textFieldUser\" was not injected: check your FXML file 'main-page.fxml'.";
        assert listView != null : "fx:id=\"listView\" was not injected: check your FXML file 'list-page.fxml'.";

        certificates = new Certificates();
    }

}