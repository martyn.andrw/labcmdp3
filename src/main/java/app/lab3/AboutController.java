package app.lab3;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AboutController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    private Stage mainStage;
    private Stage thisStage;

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    public void setThisStage(Stage thisStage) {
        this.thisStage = thisStage;
    }

    @FXML
    void onActionAbout(ActionEvent event) {

    }

    @FXML
    void onActionBack(ActionEvent event) {
        if (mainStage.isShowing()) {
            thisStage.close();
        } else {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(MainApplication.class.getResource("main-page.fxml"));
                Scene scene = new Scene(fxmlLoader.load());

                MainController mainController = fxmlLoader.getController();
                mainController.setMainStage(mainStage);

                mainStage.setTitle("Создание и просмотр подписанных документов");
                mainStage.setScene(scene);

                mainStage.show();
                thisStage.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    void onActionExit(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    void initialize() {

    }

}
