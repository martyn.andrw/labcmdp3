module app.lab3 {
    requires javafx.controls;
    requires javafx.fxml;


    opens app.lab3 to javafx.fxml;
    exports app.lab3;
}